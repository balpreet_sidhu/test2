/* Name: Balpreet Singh Sidhu
 * Student ID: C0748028
 */
package test2;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	private static ArrayList<Shape> shapes = new ArrayList<>();
	
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;
		
		while (choice != 4) {
			// 1. show the menu
			showMenu();
	
			// 2. get the user input
			System.out.println("Enter a number: ");
			choice = keyboard.nextInt();
			
			// 3. DEBUG: Output what the user typed in 
			System.out.println("You entered: " + choice);
			
			handleChoices(choice);
			
			System.out.println();
		}
	}
	
	public static void handleChoices(int choice) 
	{
		Scanner keyboard = new Scanner(System.in);
		if (choice == 1) 
		{
			System.out.println("Please enter the colour of triangle: ");
			String color = keyboard.nextLine().trim();

			System.out.println(" value of triangle's base: ");
			double base = keyboard.nextDouble();

			System.out.println("value of triangle's height: ");
			double height = keyboard.nextDouble();
			Shape s = new Triangle(color);
			
			System.out.println("The area of your Triangle is: " + s.calculateArea());
			
			shapes.add(s);
		} 
		else if (choice == 2) 
			
		{
			System.out.println("Please enter the Square's color: ");
			String color = keyboard.nextLine().trim();

			System.out.println("Please enter the value of Square's side: ");
			double side = keyboard.nextDouble();
			Shape s = new Square(color, side);
			
			System.out.println("The area of this Square is: " + s.calculateArea());
			
			shapes.add(s);
		} 
		
		else if (choice == 3) 
		
		{
			int i = 0;
			for(Shape shape : shapes) {
				
				shape.printInfo();
				
			}
			System.out.println("you are out of the program...");
			System.exit(0);
		} 
		
		
	}
	public static void showMenu() {
		System.out.println("AREA GENERATOR");
		System.out.println("==============");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
		System.out.println("3. Exit");
	}

}